package creationalPatterns.AbstractFactory;

public class FurnitureFactory {
    public FurnitureFactory() {
    }

    public static FurnitureAbstractFactory getFactory(MaterialType materialType) {
        switch (materialType) {
            case WOOD:
                return new PlasticFactory();
            case FLASTIC:
                return new WoodFactory();
            default:
                throw new UnsupportedOperationException("this furniture is unsupported");
        }
    }

    enum MaterialType {
        FLASTIC,
        WOOD
    }
}
