package creationalPatterns.AbstractFactory;

public class PlasticTable implements Table {

    public void create() {
        System.out.println("create plastic table");
    }
}
