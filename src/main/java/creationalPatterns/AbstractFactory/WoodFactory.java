package creationalPatterns.AbstractFactory;

public class WoodFactory extends FurnitureAbstractFactory {
    public Chair createChair() {
        return new WoodChair();
    }

    public Table createTable() {
        return new WoodTable();
    }
}
