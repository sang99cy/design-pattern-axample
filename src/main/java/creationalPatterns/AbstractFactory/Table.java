package creationalPatterns.AbstractFactory;

public interface Table {
    void create();
}
