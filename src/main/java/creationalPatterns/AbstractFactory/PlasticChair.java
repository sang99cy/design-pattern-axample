package creationalPatterns.AbstractFactory;

public class PlasticChair implements Chair {
    public void create() {
        System.out.println("create plastic chair");
    }
}
