package creationalPatterns.AbstractFactory;

public class WoodChair implements Chair {
    public void create() {
        System.out.println("create wood chair");
    }
}
