package creationalPatterns.AbstractFactory;

public class WoodTable implements Table {
    public void create() {
        System.out.println("create wood table");
    }
}
