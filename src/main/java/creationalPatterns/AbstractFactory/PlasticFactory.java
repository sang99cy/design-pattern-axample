package creationalPatterns.AbstractFactory;

public class PlasticFactory extends FurnitureAbstractFactory {
    public Chair createChair() {
        return new PlasticChair();
    }

    public Table createTable() {
        return new PlasticTable();
    }
}
