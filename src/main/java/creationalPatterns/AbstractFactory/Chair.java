package creationalPatterns.AbstractFactory;

public interface Chair {
    void create();
}
