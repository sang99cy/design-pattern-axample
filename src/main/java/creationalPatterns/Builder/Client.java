package creationalPatterns.Builder;

public class Client {
    public static void main(String[] args) {
        Order order = new FastFoodOrderBuilder()
                .orderType(OrderType.ON_SITE)
                .orderBread(BreadType.BEEF)
                .orderSauce(SauceType.FISH_SAUCE)
                .orderVegetable(VegetableType.TOMATO)
                .build();
        System.out.println(order);

        BankAccount newAccount = new BankAccount
                .BankAccountBuilder("GP Coder", "0123456789")
                .withEmail("contact@gpcoder.com")
                .wantNewsletter(true)
                .build();
        System.out.println(newAccount);
    }
}
