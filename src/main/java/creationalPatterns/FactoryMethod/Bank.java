package creationalPatterns.FactoryMethod;

/*super class*/
public interface Bank {
    String getBankName();
}
