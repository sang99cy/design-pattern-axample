package creationalPatterns.FactoryMethod;

public class VietcomBank implements Bank {
    public String getBankName() {
        return "VietcomBank";
    }
}
