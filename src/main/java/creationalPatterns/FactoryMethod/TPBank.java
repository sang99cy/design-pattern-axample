package creationalPatterns.FactoryMethod;

public class TPBank implements Bank {
    public String getBankName() {
        return "TPBank";
    }
}
