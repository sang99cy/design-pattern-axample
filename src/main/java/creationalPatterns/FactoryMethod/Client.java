package creationalPatterns.FactoryMethod;

public class Client {
    public static void main(String[] args) {
        Bank bank = BankFactory.getBank(BankFactory.BankType.TPBANK);
        System.out.println(bank.getBankName());
    }
}
